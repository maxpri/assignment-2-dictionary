extern string_equals

global find_word

section .text

find_word:
	.loop:
		cmp rsi, 0
		je .not_found

		push rsi
		push rdi
		add rsi, 8
		call string_equals
		pop rdi
		pop rsi

		cmp rax, 1
		je .found

		mov rsi, [rsi]
		jmp .loop

	.not_found:
		xor rax, rax
		ret

	.found:
		mov rax, rsi
		ret
